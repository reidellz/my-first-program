package Employees;

public class SetDisplaEmployees {

	public static void main(String[] args) {
		
		Employees clerk = new Employees();
		Employees driver = new Employees();
		Employees manager = new Employees();
		Employees clerk2 = new Employees();
		
		//SET A VALUE
		clerk.setEmployeeNumber(3567);
		driver.setEmployeeNumber(3497);
		manager.setEmployeeNumber(8000);
		clerk2.setEmployeeNumber(2009);
		
		clerk.setEmployeeFirstName("Andy");
		clerk.setEmployeeLastName("Smith");
		
		driver.setEmployeeFirstName("Aaron");
		driver.setEmployeeLastName("Rogers");
		
		manager.setEmployeeFirstName("Zach");
		manager.setEmployeeLastName("Reidell");
		
		clerk2.setEmployeeFirstName("Anthony");
		clerk2.setEmployeeLastName("Hernandez");
		
		manager.setEmployeeLocation("Pittsburgh");
		
		
		//Get Information
		System.out.println("The Manager Employee Number is " + manager.getEmployeeNumber());
		System.out.println("The Managers name is " + manager.getEmployeeFirstName() + " " + manager.getEmployeeLastName());
		System.out.println("The Managers location is " + manager.getEmployeeLocation());
		
		
	}

}
