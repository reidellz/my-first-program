package application;


//Created By: Zach Reidell 
//Modified By: Zach Reidell
//Date Created: 5/05/2018
//Date Modified: 5/05/2018
//Description: This is the main class for the application. 
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;




public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("application.fxml"));
		primaryStage.setTitle("Janets Favorite Movies");
		primaryStage.setScene(new Scene(root, 800, 600));
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
