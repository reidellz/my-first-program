package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static java.awt.SystemColor.text;

import java.awt.event.ActionEvent;

 


public class Controller {
	
ObservableList<MovieNames> movies = FXCollections.observableArrayList();
File f = new File("movies.txt");

@FXML
public ListView<MovieNames> list;

@FXML
public TextField name;

@FXML
public TextField genre;

@FXML
public TextField rating;


@FXML
public Button AddBtn;

//Calling a method to add a movie through name, genre, and rating to the list of movies. 
public void addMovie () throws IOException {
	MovieNames moviename = new MovieNames (name.getText(), genre.getText(), rating.getText());
	movies.add(moviename);
	name.setText("");
	genre.setText("");
	rating.setText("");
	list.getItems();
	

	
	
	
	FileWriter fw = new FileWriter(f); 
	fw.append(moviename.toString());
	fw.close();
	
}




//Calling a method to remove a movie on the list of movies. 
public void removeMovie() {
	MovieNames moviename = (MovieNames) list.getSelectionModel().getSelectedItem();
	movies.remove(moviename);
}
	
	
	
public void initialize(URL location, ResourceBundle resources) {
	list.setItems(movies);
}
}


