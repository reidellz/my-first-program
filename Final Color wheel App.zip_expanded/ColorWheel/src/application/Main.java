package application;

/**
* Author: Zach Reidell, Domenic Petrarca, Shane Kengerski, Lauren Genter
* Created Date: 5/9/18
* Modified Date: 5/9/18
*Description: This is our CIT-111 Final project for a color quiz.
**/


import java.awt.Insets;
import java.io.IOException;

import application.Main;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;




public class Main extends Application {

	private  static Stage primaryStage;
	private static BorderPane mainLayout;
	
//Start override method
	@Override
	public void start(Stage inputStage) throws Exception {
			 
		
		primaryStage = inputStage;
		primaryStage.setTitle("SplashScreen");
		showSplashScreen();		
	}
	
	
//Method to show Splash Screen/Primary Stage	
	private void showSplashScreen() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("Scenes/SplashScreen.fxml"));
		mainLayout = loader.load(); 
		Scene scene = new Scene (mainLayout);
		primaryStage.setScene(scene);
		primaryStage.show();		
	}	

	
//Method to show Home Screen
	public static void showHomeScreen() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("Scenes/HomeScreen.fxml"));
		BorderPane HomeScreen = loader.load();
		mainLayout.setTop(HomeScreen);
		
	}
	
	
//Method to show Help Screen	
	public static void showHelpScreen() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("Scenes/HelpScreen.fxml"));
		BorderPane HelpScreen = loader.load();
		mainLayout.setTop(HelpScreen);
			 
	}
	
	
//Method to show Quiz Screen	
public static void showQuizScreen() throws IOException{
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("Scenes/QuizScreen.fxml"));
		BorderPane QuizScreen = loader.load();
		mainLayout.setTop(QuizScreen);
			 
	}


//Method to show Results Screen
public static void showResultsScreen() throws IOException{
	
	
	FXMLLoader loader = new FXMLLoader();
	loader.setLocation(Main.class.getResource("Scenes/ResultsScreen.fxml"));
	BorderPane ResultsScreen = loader.load();
	mainLayout.setTop(ResultsScreen);
		 
}
	
	public static void main (String[] args)
	{
		launch(args);
	}

	

}