package application.Scenes;
/**
* Author: Zach Reidell, Domenic Petrarca, Shane Kengerski, Lauren Genter
* Created Date: 5/9/18
* Modified Date: 5/9/18
*Description: This is our CIT-111 Final project for a color quiz.
**/
import java.io.IOException;
import application.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class SplashScreenController {

	@FXML
	private Button Enter;
	
	//Telling the Enter button to go to HomeScreen and start the program
	@FXML
	private void goHome() throws IOException{
		Main.showHomeScreen();
	}

	
}
