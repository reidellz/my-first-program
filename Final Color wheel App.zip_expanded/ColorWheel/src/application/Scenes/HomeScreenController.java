package application.Scenes;

/**
* Author: Zach Reidell, Domenic Petrarca, Shane Kengerski, Lauren Genter
* Created Date: 5/9/18
* Modified Date: 5/9/18
*Description: This is our CIT-111 Final project for a color quiz.
**/



import java.awt.Container;
import javafx.event.ActionEvent;
//import java.awt.event.ActionListener;
import javafx.event.EventHandler;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import application.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;



public class HomeScreenController implements Initializable {




	@FXML
	private Button Help;
	@FXML
	private Button Submit;
	@FXML
	private Button Quiz;
	@FXML
	public ComboBox<String> comboBox1;
	@FXML
	public ComboBox<String> comboBox2;
	@FXML
	private Label Screen;
	 //final TextField subject = new TextField("");
	
	ObservableList<String> colorList = FXCollections.observableArrayList("Red", "Blue", "Yellow");

	ObservableList<String> colorList2 = FXCollections.observableArrayList("Red", "Blue", "Yellow");

		
	
	//Method to go to the help screen
	@FXML
	private void goHelpScreen() throws IOException{
		Main.showHelpScreen();
	}
    //Method to go to the quiz screen
	@FXML
	private void goQuizScreen() throws IOException{
		Main.showQuizScreen();
	}

	// Initialize method 
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		comboBox1.setValue("Red");
		comboBox1.setItems(colorList);

			
		comboBox2.setValue("Red");
		comboBox2.setItems(colorList2);

		Submit.setOnAction(new EventHandler<ActionEvent>() {
			
			// Action Event method with the combo boxes
		    @Override
		    public void handle(ActionEvent e) { 
		        if (comboBox1.getValue() != null && comboBox2.getValue() != null) {
		        	mixColors(comboBox1.getValue(), comboBox2.getValue());
		        }
		        else {
		        	Screen.setText("You have not selected an option!"); 
		        }
		        
		    }
		});	
	}
		//Method to string the combo boxes to give one result 
	private void mixColors(String color1, String color2)
	{
		if(color1 == "Red" ) {
			if(color2 == "Red" ) {
				Screen.setText("Red + Red = Red!");
			} else if(color2 == "Blue" ) {
				Screen.setText("Red + Blue = Purple!");				
			} else if(color2 == "Yellow" ) {
				Screen.setText("Red + Yellow = Orange!");
			}
			
		} else if(color1 == "Blue" ) {
			if(color2 == "Red" ) {
				Screen.setText("Blue + Red = Purple!");
			} else if(color2 == "Blue" ) {
				Screen.setText("Blue + Blue = Blue!");				
			} else if(color2 == "Yellow" ) {
				Screen.setText("Blue + Yellow = Green!");
			}
		} else if(color1 == "Yellow" ) {
			if(color2 == "Red" ) {
				Screen.setText("Yellow + Red = Orange!");
			} else if(color2 == "Blue" ) {
				Screen.setText("Yellow + Blue = Green!");				
			} else if(color2 == "Yellow" ) {
				Screen.setText("Yellow + Yellow = Yellow!");
			}		
		}
	
	}

}

