package application.Scenes;
/**
* Author: Zach Reidell, Domenic Petrarca, Shane Kengerski, Lauren Genter
* Created Date: 5/9/18
* Modified Date: 5/9/18
*Description: This is our CIT-111 Final project for a color quiz.
**/
import java.io.IOException;

import application.Main;
import javafx.fxml.FXML;
public class QuizScreenController {

	
	@FXML
	private void goHome() throws IOException{
		Main.showHomeScreen();
	}
	
	@FXML
	private void goResults() throws IOException{
		Main.showResultsScreen();
	}
}
